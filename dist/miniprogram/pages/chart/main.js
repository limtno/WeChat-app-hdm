require("../../common/manifest.js");
require("../../common/vendor.js");
global.webpackJsonp([2],{

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(32);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/***/ }),

/***/ 32:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_3c41429b_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(74);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(33)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_3c41429b_hasScoped_false_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/chart/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3c41429b", Component.options)
  } else {
    hotAPI.reload("data-v-3c41429b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 33:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 34:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_values__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_values___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_values__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_mpvue_f2__ = __webpack_require__(62);


//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


var F2 = __webpack_require__(73);
var db = wx.cloud.database();
var _ = db.command;

var chart = null;

function initChart(canvas, width, height) {
  chart = new F2.Chart({
    el: canvas,
    width: width,
    height: height
  });

  chart.tooltip({
    custom: true,
    showCrosshairs: true,
    onChange: function onChange(obj) {
      var legend = chart.get("legendController").legends.top[0];
      var tooltipItems = obj.items;
      var legendItems = legend.items;
      var map = {};
      legendItems.map(function (item) {
        map[item.name] = __WEBPACK_IMPORTED_MODULE_1_babel_runtime_core_js_object_assign___default()({}, item);
      });
      tooltipItems.map(function (item) {
        var name = item.name,
            value = item.value;

        if (map[name]) {
          map[name].value = value;
        }
      });
      legend.setItems(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_core_js_object_values___default()(map));
    },
    onHide: function onHide() {
      var legend = chart.get("legendController").legends.top[0];
      legend.setItems(chart.getLegendItems().country);
    }
  });

  // chart.guide().rect({
  //   start: [3/2, "max"],
  //   end: ["max", "min"],
  //   style: {
  //     fill: "#CCD6EC",
  //     opacity: 0.3
  //   }
  // });
  // chart.guide().text({
  //   position: [3/2, "max"],
  //   content: "1",
  //   style: {
  //     fontSize: 10,
  //     textBaseline: "top"
  //   }
  // });

  chart.line().position("day*value").color("type", function (val) {
    if (val === "平均(kg)") {
      return "#CCD6EC";
    } else if (val === "体重(kg)") {
      return "#7acfdf";
    } else if (val === "收缩压(mmHg)") {
      return "#f9ad8d";
    } else if (val === "舒张压(mmHg)") {
      return "#f47645";
    } else if (val === "脉搏(bpm)") {
      return "#b73535";
    }
  });

  return chart;
}

/* harmony default export */ __webpack_exports__["a"] = ({
  components: {
    mpvueF2: __WEBPACK_IMPORTED_MODULE_2_mpvue_f2__["a" /* default */]
  },
  data: function data() {
    return {
      weightAve: 0,
      weightMax: 0,
      weightMin: 0,
      DPAve: 0,
      DPMax: 0,
      DPMin: 0,
      SPAve: 0,
      SPMax: 0,
      SPMin: 0,
      PulseAve: 0,
      PulseMax: 0,
      PulseMin: 0,
      scrollHeight: 0,
      f2: F2,
      onInit: initChart,
      mode: 10,
      todayData: [2019 / 1 / 1],
      userData: []
    };
  },
  onLoad: function onLoad() {
    var _this = this;

    var query = this.$root.$mp.query;

    this.mode = query.mode;
    this.todayData = query.todayData;
    this.getDate();
    //获取f2标签高度

    wx.getSystemInfo({
      success: function success(res) {
        console.log(res);
        var windowHeight = res.windowHeight;
        var a = 750 / res.windowWidth;
        _this.scrollHeight = windowHeight - 600 / a;
      }
    });
  },

  methods: {
    getDate: function getDate() {
      var _this2 = this;

      var dateMax = new Date(this.todayData + " " + 23 + ":" + 59 + ":" + 59);
      console.log(dateMax);
      db.collection("userData").orderBy("mode", "asc").orderBy("time", "asc").where({
        mode: parseInt(this.mode),
        _openid: wx.getStorageSync('openid')
        //time:_.gte(dateMax),
        //date:_.gt(dateMin)
      }).get({
        success: function success(res) {
          var userData = res.data;
          console.log(userData);
          //转换日期
          for (var i = 0; i < userData.length; i++) {
            var Y = userData[i].time.getFullYear() + "/";
            var M = userData[i].time.getMonth() + 1 + "/";
            var D = userData[i].time.getDate() + " ";
            var h = userData[i].time.getHours() + ":";
            var m = userData[i].time.getMinutes();
            var s = userData[i].time.getSeconds();
            userData[i].time = M + D;
          }
          _this2.userData = res.data;
          _this2.chuli();
        }
      });
    },
    chuli: function chuli() {
      var ChartData = [];
      var ChartTicks = [];

      var userData = this.userData;

      if (this.mode == 0) {
        var weightSum = 0;
        var weightData = [];
        //计算平均数
        for (var i = 0; i < userData.length; i++) {
          weightSum = weightSum + parseInt(userData[i].weight);
        }
        this.weightAve = (weightSum / userData.length).toFixed(1);
        //push数据
        for (var i = 0; i < userData.length; i++) {
          weightData.push(userData[i].weight);
          //push日期
          ChartTicks.push(userData[i].time);
          ChartData.push({
            day: userData[i].time,
            type: "体重(kg)",
            value: parseInt(userData[i].weight)
          });
          ChartData.push({
            day: userData[i].time,
            type: "平均(kg)",
            value: weightSum / userData.length
          });
          console.log(ChartTicks);
        }
        //计算最大值
        this.weightMax = Math.max.apply(Math, weightData);
        //计算最小值
        this.weightMin = Math.min.apply(Math, weightData);
      } else {
        var DPSum = 0;
        var SPSum = 0;
        var PulseSum = 0;
        var DPData = [];
        var SPData = [];
        var PulseData = [];
        for (var i = 0; i < userData.length; i++) {
          //push日期
          ChartTicks.push(userData[i].time);
          ChartData.push({
            day: userData[i].time,
            type: "舒张压(mmHg)",
            value: parseInt(userData[i].SP)
          });
          ChartData.push({
            day: userData[i].time,
            type: "收缩压(mmHg)",
            value: parseInt(userData[i].DP)
          });
          ChartData.push({
            day: userData[i].time,
            type: "脉搏(bpm)",
            value: parseInt(userData[i].Pulse)
          });

          //计算总和
          DPSum = DPSum + parseInt(userData[i].DP);
          SPSum = SPSum + parseInt(userData[i].SP);
          PulseSum = PulseSum + parseInt(userData[i].Pulse);
          //push数组
          DPData.push(userData[i].DP);
          SPData.push(userData[i].SP);
          PulseData.push(userData[i].Pulse);
        }
        //计算平均值
        this.DPAve = (DPSum / userData.length).toFixed(0);
        this.SPAve = (SPSum / userData.length).toFixed(0);
        this.PulseAve = (PulseSum / userData.length).toFixed(0);
        //计算最大值
        this.DPMax = Math.max.apply(Math, DPData);
        this.SPMax = Math.max.apply(Math, SPData);
        this.PulseMax = Math.max.apply(Math, PulseData);
        //计算最小值
        this.DPMax = Math.min.apply(Math, DPData);
        this.SPMax = Math.min.apply(Math, SPData);
        this.PulseMax = Math.min.apply(Math, PulseData);
      }

      var data = ChartData;
      chart.source(data, {
        day: {
          range: [0, 1],
          ticks: ChartTicks
        },
        value: {
          tickCount: 10,
          formatter: function formatter(val) {
            return val.toFixed(0);
          }
        }
      });
      chart.render();
      //option.series = series;
      //option.xAxis = xAxis;
      //chart.setOption(option);
    }
  }
});

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_mpvue_loader_lib_selector_type_script_index_0_f2_vue__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mpvue_loader_lib_template_compiler_index_id_data_v_3a4bc82b_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_mpvue_loader_lib_selector_type_template_index_0_f2_vue__ = __webpack_require__(72);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(63)
}
var normalizeComponent = __webpack_require__(1)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3a4bc82b"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_mpvue_loader_lib_selector_type_script_index_0_f2_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__mpvue_loader_lib_template_compiler_index_id_data_v_3a4bc82b_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_mpvue_loader_lib_selector_type_template_index_0_f2_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "node_modules/mpvue-f2/src/f2.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] f2.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3a4bc82b", Component.options)
  } else {
    hotAPI.reload("data-v-3a4bc82b", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 63:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_renderer__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lib_f2__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__lib_f2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__lib_f2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__interaction_index__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__interaction_index___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__interaction_index__);

//
//
//
//
//
//
//
//
//
//
//
//
//




// override
__WEBPACK_IMPORTED_MODULE_2__lib_f2___default.a.Util.measureText = function (text, font) {
  ctx.font = font || '12px sans-serif';
  return ctx.measureText(text);
};

__WEBPACK_IMPORTED_MODULE_2__lib_f2___default.a.Util.addEventListener = function (source, type, listener) {
  source.addListener(type, listener);
};

__WEBPACK_IMPORTED_MODULE_2__lib_f2___default.a.Util.removeEventListener = function (source, type, listener) {
  source.removeListener(type, listener);
};

__WEBPACK_IMPORTED_MODULE_2__lib_f2___default.a.Util.createEvent = function (event, chart) {
  var type = event.type;
  var x = 0;
  var y = 0;
  var touches = event.touches;
  if (touches && touches.length > 0) {
    x = touches[0].x;
    y = touches[0].y;
  }

  return {
    type: type,
    chart: chart,
    x: x,
    y: y
  };
};

/* harmony default export */ __webpack_exports__["a"] = ({
  props: {
    f2: {
      required: true,
      type: Object,
      default: function _default() {
        return null;
      }
    },
    onInit: {
      required: true,
      type: Function,
      default: null
    },
    canvasId: {
      type: String,
      default: 'f2-canvas'
    },
    lazyLoad: {
      type: Boolean,
      default: false
    }
  },
  onReady: function onReady() {
    if (!this.f2) {
      console.warn('组件需绑定 f2 变量，例：<mpvue-f2 id="mychart-dom-bar" :onInit="onInit" ' + 'canvas-id="mychart-bar" :f2="f2"></mpvue-f2>');
      return;
    }

    if (!this.lazyLoad) this.init();
  },

  methods: {
    init: function init() {
      var _this = this;

      var version = wx.version.version.split('.').map(function (n) {
        return parseInt(n, 10);
      });
      var isValid = version[0] > 1 || version[0] === 1 && version[1] > 9 || version[0] === 1 && version[1] === 9 && version[2] >= 91;
      if (!isValid) {
        console.error('微信基础库版本过低，需大于等于 1.9.91。' + '参见：https://github.com/antvis/wx-f2#%E5%BE%AE%E4%BF%A1%E7%89%88%E6%9C%AC%E8%A6%81%E6%B1%82');
        return;
      }

      if (!this.onInit) {
        console.warn('请传入 onInit 函数进行初始化');
        return;
      }

      var canvasId = this.canvasId;

      this.ctx = wx.createCanvasContext(canvasId);

      var canvas = new __WEBPACK_IMPORTED_MODULE_1__lib_renderer__["a" /* default */](this.ctx);
      this.canvas = canvas;

      var query = wx.createSelectorQuery();
      query.select('#' + canvasId).boundingClientRect(function (res) {
        if (!res) {
          setTimeout(function () {
            return _this.init();
          }, 50);
          return;
        }
        if (typeof callback === 'function') {
          _this.chart = callback(canvas, res.width, res.height);
        } else if (_this.onInit) {
          _this.chart = _this.onInit(canvas, res.width, res.height);
        }
      }).exec();
    },
    canvasToTempFilePath: function canvasToTempFilePath(opt) {
      var canvasId = this.canvasId;

      this.ctx.draw(true, function () {
        wx.canvasToTempFilePath(__WEBPACK_IMPORTED_MODULE_0_babel_runtime_helpers_extends___default()({
          canvasId: canvasId
        }, opt));
      });
    },
    touchStart: function touchStart(e) {
      if (this.canvas) {
        this.canvas.emitEvent('touchstart', [e]);
      }
    },
    touchMove: function touchMove(e) {
      if (this.canvas) {
        this.canvas.emitEvent('touchmove', [e]);
      }
    },
    touchEnd: function touchEnd(e) {
      if (this.canvas) {
        this.canvas.emitEvent('touchend', [e]);
      }
    },
    press: function press(e) {
      if (this.canvas) {
        this.canvas.emitEvent('press', [e]);
      }
    }
  }
});

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.canvasId) ? _c('canvas', {
    staticClass: "f2-canvas",
    attrs: {
      "id": _vm.canvasId,
      "canvasId": _vm.canvasId,
      "eventid": '0'
    },
    on: {
      "touchstart": _vm.touchStart,
      "touchmove": _vm.touchMove,
      "touchend": _vm.touchEnd,
      "error": _vm.error
    }
  }) : _vm._e()
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3a4bc82b", esExports)
  }
}

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "echarts-wrap"
  }, [_c('mpvue-f2', {
    attrs: {
      "f2": _vm.f2,
      "onInit": _vm.onInit,
      "canvasId": "demo-canvas",
      "mpcomid": '0'
    }
  }), _vm._v(" "), (_vm.userData) ? _c('scroll-view', {
    style: ({
      height: _vm.scrollHeight + 'px'
    }),
    attrs: {
      "scroll-y": ""
    }
  }, [_c('div', {
    staticClass: "dataBar",
    attrs: {
      "id": "f2"
    }
  }, [(_vm.mode == 0) ? _c('div', [_c('div', {
    staticClass: "tiele"
  }, [_vm._v("体重")]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex flex"
  }, [_c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top weight"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.weightMax))]), _vm._v(" "), _c('div', {
    staticClass: "nums"
  }, [_vm._v("Kg")])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最高值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top weight"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.weightMin))]), _vm._v(" "), _c('div', {
    staticClass: "nums"
  }, [_vm._v("Kg")])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最低值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top weight"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.weightAve))]), _vm._v(" "), _c('div', {
    staticClass: "nums"
  }, [_vm._v("Kg")])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("平均值")])])])]) : _vm._e(), _vm._v(" "), (_vm.mode == 1) ? _c('div', [_c('div', {
    staticClass: "tiele"
  }, [_vm._v("舒张压")]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex flex"
  }, [_c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top SP"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.SPMax))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最高值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top SP"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.SPMin))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最低值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top SP"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.SPAve))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("平均值")])])])]) : _vm._e(), _vm._v(" "), (_vm.mode == 1) ? _c('div', [_c('div', {
    staticClass: "tiele"
  }, [_vm._v("收缩压")]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex flex"
  }, [_c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top DP"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.DPMax))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最高值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top DP"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.DPMin))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最低值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top DP"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.DPAve))])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("平均值")])])])]) : _vm._e(), _vm._v(" "), (_vm.mode == 1) ? _c('div', [_c('div', {
    staticClass: "tiele"
  }, [_vm._v("脉搏")]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex flex"
  }, [_c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top Pulse"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.PulseMax))]), _vm._v(" "), _c('div', {
    staticClass: "nums"
  }, [_vm._v("bpm")])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最高值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top Pulse"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.PulseMin))]), _vm._v(" "), _c('div', {
    staticClass: "nums"
  }, [_vm._v("bpm")])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("最低值")])]), _vm._v(" "), _c('div', {
    staticClass: "weui-flex__item item"
  }, [_c('div', {
    staticClass: "top Pulse"
  }, [_c('div', {
    staticClass: "num"
  }, [_vm._v(_vm._s(_vm.PulseAve))]), _vm._v(" "), _c('div', {
    staticClass: "nums"
  }, [_vm._v("bpm")])]), _vm._v(" "), _c('div', {
    staticClass: "bottom"
  }, [_vm._v("平均值")])])])]) : _vm._e()])]) : _vm._e()], 1)
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-3c41429b", esExports)
  }
}

/***/ })

},[31]);
//# sourceMappingURL=main.js.map